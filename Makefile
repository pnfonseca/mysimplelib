
.PHONY = run

UNAME := $(shell uname)

# The following lines allow to define environment variables
# according to the system where the Makefile is run.
# In this casa, a different set of variables is defined  depending
# on the Makefile being run on Windows (Cygwin) or Linux
ifeq ($(UNAME), Linux)
PIC32_HOME = /opt/microchip
PROG = main
endif

ifeq ($(UNAME),CYGWIN_NT-6.1)
PIC32_HOME = /cygdrive/c/Program\ Files\ \(x86\)/Microchip
PROG = main.exe
endif

CC = gcc
CFLAGS +=  -D__32MX795F512H__ -D__LANGUAGE_C__
CPPFLAGS += -I $(PIC32_HOME)/xc32/v1.42/pic32mx/include/

$(PROG) : main.o mem.o
	gcc main.o mem.o -o main

run : $(PROG)
	./$(PROG)
	
clean : 
	rm -f *.o
	rm -f *~

cleanall : clean
	rm -f $(PROG)
