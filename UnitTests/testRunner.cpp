/*
 * testRunner.cpp
 *
 *  Created on: Dec 9, 2012
 *      Author: pf
 */


 #include <CppUTest/TestHarness.h>

extern "C" {
#include "../mysimplelib.h"
#include "p32mx795f512h.h"
}



TEST_GROUP(TimerTest)
{
	void setup()
	{

	}

	void teardown()
	{

	}
};


TEST(TimerTest,TestSet){
	/* Clear all bits in T2CON */
	T2CON = 0x0000;
	
	Timer2control(TOn);
	
	CHECK_EQUAL(0x8000, T2CON);
	
}