#include <xc.h>

#include "mysimplelib.h"

void Timer2control(TimerStates_t T2state)
{
	switch(T2state){
		case TOn :
			T2CONbits.ON = 1;
			break;
			
		case TOff:
			T2CONbits.ON = 0;			
			break;
	}
	
	return;
}
